import { Component } from '@angular/core'
import { Platform, App } from 'ionic-angular'
import { StatusBar } from 'ionic-native'
import { AngularFire } from 'angularfire2'

import { LandingPage } from '../pages/auth/landing/landing'
import { SideMenuPage } from '../pages/sideMenu/sideMenu'

@Component({
  template: `<ion-nav></ion-nav>`
})
export class MyApp {
  constructor(platform: Platform, public app: App, public af: AngularFire) {
    platform.ready().then(() => {
      this.initAuth()
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault()
    })
  }

  initAuth() {
    // Fire up the auth service, this is how the ion-nav root
    // component is set. For example, if your already logged in
    // the tabs controller will be loaded instead of the login page.
    this.af.auth.subscribe(user => {
      if (user) {
        this.app.getRootNav().setRoot(SideMenuPage)
      } else {
        this.app.getRootNav().setRoot(LandingPage)
      }
    })
  }
}

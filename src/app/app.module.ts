import { NgModule } from '@angular/core'
import { IonicApp, IonicModule } from 'ionic-angular'
import { AngularFireModule, AuthProviders, AuthMethods } from 'angularfire2'

import { MyApp } from './app.component'
import { DbService } from '../services/db.service'
import { AuthService } from '../services/auth.service'
import { ToastService } from '../services/toast.service'
import { LandingPage } from '../pages/auth/landing/landing'
import { ManageCouponPage } from '../pages/manageCoupon/manageCoupon'
import { LoginPage } from '../pages/auth/login/login'
import { AddCouponPage } from '../pages/addCoupon/addCoupon'
import { CreateAccountPage } from '../pages/auth/createAccount/createAccount'
import { HistoryPage } from '../pages/history/history'
import { ScanPage } from '../pages/scan/scan'
import { AccountPage } from '../pages/account/account'
import { CouponsPage } from '../pages/coupons/coupons'
import { CouponSinglePage } from '../pages/couponSingle/couponSingle'
import { SideMenuPage } from '../pages/sideMenu/sideMenu'

export const firebaseConfig = {
  apiKey: 'AIzaSyC8ltx3KdlLjEePMjeXZQL384CmTJbY9_4',
  authDomain: 'coupon-starter.firebaseapp.com',
  databaseURL: 'https://coupon-starter.firebaseio.com',
  storageBucket: 'coupon-starter.appspot.com'
}

const myFirebaseAuthConfig = {
  provider: AuthProviders.Password,
  method: AuthMethods.Password
}

@NgModule({
  declarations: [
    MyApp,
    LandingPage,
    LoginPage,
    AddCouponPage,
    CreateAccountPage,
    ManageCouponPage,
    HistoryPage,
    ScanPage,
    AccountPage,
    CouponsPage,
    CouponSinglePage,
    SideMenuPage
  ],
  imports: [
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig, myFirebaseAuthConfig)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LandingPage,
    LoginPage,
    AddCouponPage,
    CreateAccountPage,
    ManageCouponPage,
    HistoryPage,
    ScanPage,
    AccountPage,
    CouponsPage,
    CouponSinglePage,
    SideMenuPage
  ],
  providers: [AuthService, ToastService, DbService]
})
export class AppModule {}

import { Injectable } from '@angular/core'
import { AngularFire } from 'angularfire2'

import { ToastService } from './toast.service'
import { DbService } from './db.service'

@Injectable()
export class AuthService {
  constructor(
    public toastService: ToastService,
    public af: AngularFire,
    public dbService: DbService
  ) {}

  login(email, password) {
    this.af.auth.login({email: email, password: password})
      .then((res) => {
        this.dbService.loadUser(res.uid)
      })
      .catch((error) => {
        this.toastService.serve(error.message)
        console.log(error.message)
      })
  }

  logout() {
    this.af.auth.logout()
  }

  createUser(email, password) {
    this.af.auth.createUser({email: email, password: password})
      .then((res) => {
        this.dbService.createUser(res)
      })
      .catch((error) => {
        this.toastService.serve(error.message)
        console.log(error.message)
      })
  }
}

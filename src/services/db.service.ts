import { Injectable } from '@angular/core'
import { AngularFire } from 'angularfire2'

@Injectable()
export class DbService {
  usersObject
  couponsList
  user = {}

  constructor(public af: AngularFire) {
    this.usersObject = this.af.database.object('/users')
    this.couponsList = this.af.database.list('/coupons')
  }

  createUser(res) {
    this.af.database.object(`/users/${ res.uid }`)
      .set({
        email: res.auth.email,
        role: "user"
      })
  }

  loadUser(uid) {
    let userObj = this.af.database.object(`/users/${ uid }`)

    this.user = userObj

    // userObj.subscribe((snapshot) => {
    //   this.user = snapshot.val()
    //   console.log('db service' + JSON.stringify(this.user))
    // })
  }

  removeCoupon(key) {
    this.couponsList.remove(key)
  }

  createCoupon(coupon) {
    this.couponsList.push(coupon)
  }

  getCoupons() {
    return this.couponsList
  }
}

/*
class MyComp {
  questions: FirebaseListObservable<any[]>
  value: FirebaseObjectObservable<any>
  constructor(af: AngularFire) {
    this.questions = af.database.list('/questions')
    this.value = af.database.object('/value')
  }
  addToList(item: any) {
    this.questions.push(item)
  }
  removeItemFromList(key: string) {
    this.questions.remove(key).then(_ => console.log('item deleted!'))
  }
  deleteEntireList() {
    this.questions.remove().then(_ => console.log('deleted!'))
  }
  setValue(data: any) {
    this.value.set(data).then(_ => console.log('set!'))
  }
  updateValue(data: any) {
    this.value.update(data).then(_ => console.log('update!'))
  }
  deleteValue() {
    this.value.remove().then(_ => console.log('deleted!'))
  }
}
*/

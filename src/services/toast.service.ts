import { Injectable } from '@angular/core'
import { ToastController } from 'ionic-angular'

@Injectable()
export class ToastService {
  constructor(public toastCtrl: ToastController) {}

  serve(message) {
    let toast = this.toastCtrl.create({
      message: message,
      duration: 5000
    })
    toast.present()
  }
}

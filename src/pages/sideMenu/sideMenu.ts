import { Component, ViewChild } from '@angular/core'
import { Nav } from 'ionic-angular'

import { CouponsPage } from '../coupons/coupons'
import { HistoryPage } from '../history/history'
import { ScanPage } from '../scan/scan'
import { AccountPage } from '../account/account'
import { AddCouponPage } from '../addCoupon/addCoupon'

@Component({
  templateUrl: 'sideMenu.html'
})
export class SideMenuPage {
  @ViewChild(Nav) nav: Nav
  rootPage = CouponsPage
  pages

  constructor() {
    this.pages = [
      { title: 'My Coupons', component: CouponsPage },
      { title: 'History', component: HistoryPage },
      { title: 'Create Coupon', component: AddCouponPage },
      { title: 'Scan coupon', component: ScanPage },
      { title: 'My Account', component: AccountPage }
    ]
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component)
  }
}

import { Component } from '@angular/core'
import { NavController, NavParams } from 'ionic-angular'

import { DbService } from '../../services/db.service'

@Component({
  selector: 'page-manageCoupon',
  templateUrl: 'manageCoupon.html'
})
export class ManageCouponPage {
  coupon

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public dbService: DbService
  ) {
    this.coupon = navParams.get('coupon')
  }

  backButton() {
    this.navCtrl.pop()
  }

  deleteButton() {
    this.dbService.removeCoupon(this.coupon.$key)
    this.navCtrl.pop()
  }
}

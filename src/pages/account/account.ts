import { Component } from '@angular/core'

import { AuthService } from '../../services/auth.service'

@Component({
  selector: 'page-account',
  templateUrl: 'account.html'
})
export class AccountPage {
  constructor(public authService: AuthService) {}

  signOutButton() {
    this.authService.logout()
  }
}

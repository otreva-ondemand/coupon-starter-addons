import { Component } from '@angular/core'

@Component({
  selector: 'page-scan',
  templateUrl: 'scan.html'
})
export class ScanPage {
  model = {
    couponCode: ''
  }

  activateCouponButton() {
    console.log(this.model.couponCode)
    this.model.couponCode = ''
  }
}

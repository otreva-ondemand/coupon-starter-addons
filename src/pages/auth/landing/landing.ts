import { Component } from '@angular/core'
import { NavController } from 'ionic-angular'

import { CreateAccountPage } from '../createAccount/createAccount'
import { LoginPage } from '../login/login'

@Component({
  selector: 'page-landing',
  templateUrl: 'landing.html'
})
export class LandingPage {
  constructor(public navCtrl: NavController) {}

  loginButton() {
    this.navCtrl.push(LoginPage)
  }

  createAccountButton() {
    this.navCtrl.push(CreateAccountPage)
  }
}

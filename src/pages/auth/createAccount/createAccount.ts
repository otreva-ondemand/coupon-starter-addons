import { Component } from '@angular/core'

import { AuthService } from '../../../services/auth.service'
import { ToastService } from '../../../services/toast.service'

@Component({
  selector: 'page-createAccount',
  templateUrl: 'createAccount.html'
})
export class CreateAccountPage {
  model = {
    email: '',
    password: '',
    confirm: ''
  }

  constructor(
    public authService: AuthService,
    public toastService: ToastService
  ) {}

  createAccountButton() {
    if (this.model.password !== this.model.confirm) {
      this.toastService.serve('Your passwords do not match')
    } else {
      this.authService.createUser(this.model.email, this.model.password)
    }
  }
}

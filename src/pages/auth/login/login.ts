import { Component } from '@angular/core'

import { AuthService } from '../../../services/auth.service'

@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  model = {
    email: '',
    password: ''
  }

  constructor(public authService: AuthService) {}

  loginButton() {
    this.authService.login(this.model.email, this.model.password)
  }
}

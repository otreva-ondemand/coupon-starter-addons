import { Component } from '@angular/core'

import { DbService } from '../../services/db.service'

@Component({
  selector: 'page-addCoupon',
  templateUrl: 'addCoupon.html'
})
export class AddCouponPage {
  date = new Date()
  todaysDate = this.date.getFullYear() + '-' +
    ('0' + (this.date.getMonth() + 1)).slice(-2) + '-' +
    ('0' + this.date.getDate()).slice(-2)
  dateInOneYear = (this.date.getFullYear() + 1) + '-' +
    ('0' + (this.date.getMonth() + 1)).slice(-2) + '-' +
    ('0' + this.date.getDate()).slice(-2)
  dateInOneCentury = (this.date.getFullYear() + 100) + '-' +
    ('0' + (this.date.getMonth() + 1)).slice(-2) + '-' +
    ('0' + this.date.getDate()).slice(-2)
  model = {
    title: "",
    description: "",
    expDate: this.dateInOneYear,
    noExp: false
  }

  constructor(public dbService: DbService) {}

  createCouponButton() {
    this.dbService.createCoupon(this.model)
  }
}

// TODO: validate this shit, like exp date cannot be yesterday etc
// TODO: also trim whitespace

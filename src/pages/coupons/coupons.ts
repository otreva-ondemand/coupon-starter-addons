import { Component } from '@angular/core'
import { NavController } from 'ionic-angular'

import { CouponSinglePage } from '../couponSingle/couponSingle'
import { ManageCouponPage } from '../manageCoupon/manageCoupon'
import { DbService } from '../../services/db.service'

@Component({
  selector: 'page-coupons',
  templateUrl: 'coupons.html'
})
export class CouponsPage {
  coupons

  constructor(public navCtrl: NavController, public dbService: DbService) {
    this.getCoupons()
  }

  getCoupons() {
    this.coupons = this.dbService.getCoupons()
  }

  useThisCouponButton(coupon) {
    this.navCtrl.push(CouponSinglePage, { coupon: coupon })
  }

  manageThisCouponButton(coupon) {
    this.navCtrl.push(ManageCouponPage, { coupon: coupon })
  }
}
